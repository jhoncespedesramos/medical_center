<?php

namespace App\Http\Requests;

use App\Paciente;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StorePacienteRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('paciente_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'nome'           => [
                'min:8',
                'max:100',
                'required',
            ],
            'dni'           => [
                'digits:8',
                'required'
            ],
            'codigo'           => [
                'digits:6',
                'required'
            ],
            'nascimento'     => [
                'required',
                'date_format:' . config('panel.date_format'),
            ],
            'sexo'           => [
                'required',
            ],
            'area'           => [
                'string',
                'required',
            ],
            'email'   => [
                'min:7',
                'max:70',
                'email',
            ],
            'telefono'   => [
                'min:6',
                'max:12',
                'required',
            ],
            'direccion'       => [
                'min:10',
                'max:200',
                'required',
            ],
            'urban'         => [
                'min:3',
                'max:30',
                'required',
            ],
            'cidade'         => [
                'min:4',
                'max:100',
                'required',
            ],
            'estado'         => [
                'min:2',
                'max:30',
                'required',
            ],
        ];
    }
}
