<?php

return [
    'actions'                              => 'Acciones',
    'add'                                  => 'Nuevo',
    'allRightsReserved'                    => 'Todos los derechos reservados',
    'areYouSure'                           => 'Está seguro?',
    'clickHereToVerify'                    => 'Click para verificar',
    'create'                               => 'Crear',
    'dashboard'                            => 'Panel',
    'delete'                               => 'Eliminar',
    'downloadFile'                         => 'Descargar',
    'edit'                                 => 'Editar',
    'emailVerificationSuccess'             => 'E-mail verificado exitosamente.',
    'entries'                              => 'Entradas',
    'filterDate'                           => 'Filtrar por fecha',
    'forgot_password'                      => 'Olvidó la contraseña?',
    'home'                                 => 'Início',
    'list'                                 => 'Listar',
    'login'                                => 'Entrar',
    'login_email'                          => 'E-mail',
    'login_password'                       => 'Password',
    'login_password_confirmation'          => 'Confirmación de nueva contraseña',
    'logout'                               => 'Salir',
    'month'                                => 'Mes',
    'no'                                   => 'No',
    'pleaseSelect'                         => 'Seleccione por favor',
    'register'                             => 'Registrar',
    'remember_me'                          => 'Recordar',
    'reset_password'                       => 'Reiniciar contraseña',
    'save'                                 => 'Guardar',
    'search'                               => 'Buscar',
    'searching'                            => 'Buscando',
    'no_results'                           => 'Ningún resultado.',
    'results_could_not_be_loaded'          => 'Los resultados no pueden ser cargados',
    'search_input_too_short'               => 'Por favor ingrese :count o mas caractéres',
    'show'                                 => 'Mostrar',
    'systemCalendar'                       => 'Calendario',
    'thankYouForUsingOurApplication'       => 'Gracias por utilizar el sitio WEB.',
    'timeFrom'                             => 'Desde',
    'timeTo'                               => 'Hasta',
    'toggleNavigation'                     => 'Alternar navegación',
    'user_name'                            => 'Nombre',
    'verifyYourEmail'                      => 'Por favor, verifique su E-Mail.',
    'verifyYourUser'                       => 'Para finalizar su registro, active su cuenta al e-mail que le enviamos.',
    'view'                                 => 'Visualizar',
    'view_file'                            => 'Ver arquivo',
    'year'                                 => 'Año',
    'yes'                                  => 'Si',
    'youAreLoggedIn'                       => 'Sesión Iniciada',
    'yourAccountNeedsAdminApproval'        => 'Su cuenta requiere de aprobación del administrador.',
    'submit'                               => 'Enviar',
    'datatables'                           => [
        'copy'          => 'Copiar',
        'csv'           => 'CSV',
        'excel'         => 'Excel',
        'pdf'           => 'PDF',
        'print'         => 'Imprimir',
        'colvis'        => 'Columnas Visibles',
        'delete'        => 'Eliminar Selecionados',
        'zero_selected' => 'Ninguna línea seleccionada',
    ],
    'action'                               => 'Acción',
    'action_id'                            => 'ID de acción',
    'action_model'                         => 'Modelo de acción',
    'address'                              => 'Dirección',
    'administrator_can_create_other_users' => 'Administrador',
    'aggregate_function_use'               => 'Agregar función a utilizar',
    'all'                                  => 'Todos',
    'all_messages'                         => 'Todos los mensajes',
    'amount'                               => 'Cantidad',
    'answer'                               => 'Respuesta',
    'app_csv_file_to_import'               => 'Importar',
    'app_csvImport'                        => 'Importación de CSV',
    'app_file_contains_header_row'         => 'El archivo contiene una linea de encabezado?',
    'app_import_data'                      => 'Importar datos',
    'app_imported_rows_to_table'           => 'Importado :rows filas a la tabla :table',
    'app_parse_csv'                        => 'Analizar CSV',
    'asset'                                => 'Activo',
    'assets'                               => 'Activos',
    'assets_history'                       => 'Historial de activos',
    'assets_management'                    => 'Gestión de activos',
    'assigned_to'                          => 'Asignado a',
    'assigned_user'                        => 'Asignado (usuario)',
    'attachment'                           => 'Adjunto',
    'axis'                                 => 'Eje',
    'back_to_list'                         => 'Regresar',
    'basic_crm'                            => 'CRM Básico',
    'budget'                               => 'Presupuesto',
    'calendar_sources'                     => 'Fuentes de Calendario',
    'campaign'                             => 'Campaña',
    'campaigns'                            => 'Campañas',
    'categories'                           => 'Categorías',
    'category'                             => 'Categoria',
    'category_name'                        => 'Nombre da categoria',
    'change_notifications_field_1_label'   => 'Enviar notificación por e-mail a Usuario',
    'change_notifications_field_2_label'   => 'Cuando ingresar al CRUD',
    'change_password'                      => 'Cambiar contraseña',
    'chart_type'                           => 'Tipo de gráfico',
    'code'                                 => 'Código',
    'companies'                            => 'Empresas',
    'company'                              => 'Empresa',
    'company_name'                         => 'Nombre de empresa',
    'confirm_password'                     => 'Confirmación de contraseña',
    'contact_management'                   => 'Gestión de administración',
    'contacts'                             => 'Contactos',
    'content_management'                   => 'Gestión de contenido',
    'copy_paste_url_bellow'                => 'botón, copie y pegue la URL a continuación en su navegador web:',
    'country'                              => 'País',
    'coupon_management'                    => 'Gestión de cupones',
    'coupons'                              => 'Cupones',
    'coupons_amount'                       => 'Cantidad de Cupones',
    'create_new_calendar_source'           => 'Criar nova Fonte de Calendário',
    'create_new_notification'              => 'Criar nova Notificação',
    'create_new_report'                    => 'Criar novo relatório',
    'created_at'                           => 'Criado em',
    'crud_date_field'                      => 'Campo de data CRUD',
    'crud_event_field'                     => 'Campo de rótulo do evento',
    'crud_title'                           => 'Título de CRUD',
    'csv_file_to_import'                   => 'Arquivo CSV para importar',
    'csvImport'                            => 'Importação CSV',
    'current_password'                     => 'Senha atual',
    'custom_controller_index'              => 'Controller personalizada',
    'customer'                             => 'Cliente',
    'customers'                            => 'Clientes',
    'deleted_at'                           => 'Eliminado em',
    'description'                          => 'Descrição',
    'deselect_all'                         => 'Cancelar seleção',
    'discount_amount'                      => 'Quantia de desconto',
    'discount_percent'                     => 'Percentual de desconto',
    'due_date'                             => 'Data de vencimento',
    'edit_calendar_source'                 => 'Editar Fonte de Calendário',
    'email_greet'                          => 'Olá',
    'email_line1'                          => 'Você está recebendo este e-mail porque nós recebemos uma solicitação de redefinição de senha para a sua conta.',
    'email_line2'                          => 'Se você não solicitou redefinição de senha, nenhuma ação é necessária.',
    'email_regards'                        => 'Saudações',
    'end_time'                             => 'Tempo final',
    'entry_date'                           => 'Data de entrada',
    'excerpt'                              => 'Resumo',
    'faq_management'                       => 'Gestão de FAQ',
    'featured_image'                       => 'Imagem em destaque',
    'fee_percent'                          => 'Percentual de taxa',
    'file'                                 => 'Arquivo',
    'file_contains_header_row'             => 'O arquivo contém cabeçalho?',
    'first_name'                           => 'Primeiro nome',
    'group_by'                             => 'Agrupar por',
    'if_you_are_having_trouble'            => 'Se você está tendo problemas para clicar no',
    'import_data'                          => 'Importar data',
    'imported_rows_to_table'               => 'Importado :rows linhas para tabela :table',
    'inbox'                                => 'Caixa de entrada',
    'integer_float_placeholder'            => 'Por favor selecione um dos campos inteiros/float',
    'is_created'                           => 'foi criado',
    'is_deleted'                           => 'foi eliminado',
    'is_updated'                           => 'foi atualizado',
    'label_field'                          => 'Campo de rótulo',
    'last_name'                            => 'Último nome',
    'location'                             => 'Local',
    'locations'                            => 'Locais',
    'main_currency'                        => 'Moeda principal',
    'message'                              => 'Mensagem',
    'messages'                             => 'Mensagens',
    'name'                                 => 'Nome',
    'new_calendar_source'                  => 'Criar nova fonte de calendário',
    'new_message'                          => 'Nova mensagem',
    'new_password'                         => 'Nova senha',
    'no_calendar_sources'                  => 'Nenhuma fonte de calendário ainda.',
    'no_entries_in_table'                  => 'Sem registros na tabela',
    'no_reports_yet'                       => 'Nenhum relatório ainda.',
    'not_approved_p'                       => 'Sua conta não foi aprovada ainda pelo administrador, por favor, aguarde e tente mais tarde.',
    'not_approved_title'                   => 'A sua conta não está aprovada',
    'note_text'                            => 'Texto da nota',
    'notifications'                        => 'Notificações',
    'notify_user'                          => 'Notificar usuário',
    'outbox'                               => 'Caixa de saída',
    'pages'                                => 'Páginas',
    'parse_csv'                            => 'Analisar CSV',
    'permadel'                             => 'Deletar',
    'phone'                                => 'Telefone',
    'phone1'                               => 'Telefone 1',
    'phone2'                               => 'Telefone 2',
    'photo'                                => 'Foto (máx. 8 MB)',
    'photo1'                               => 'Foto1',
    'photo2'                               => 'Foto2',
    'photo3'                               => 'Foto3',
    'prefix'                               => 'Prefixo',
    'price'                                => 'Preço',
    'product_management'                   => 'Gestão de produtos',
    'product_name'                         => 'Nome do produto',
    'products'                             => 'Produtos',
    'question'                             => 'Questão',
    'questions'                            => 'Questões',
    'recipient'                            => 'Destinatário',
    'redeem_time'                          => 'Tempo de resgate',
    'registration'                         => 'Registo',
    'remember_token'                       => 'Lembrar Password',
    'reply'                                => 'Responder',
    'reports_x_axis_field'                 => 'Eixo X - por favor escolha um dos campos de data/hora',
    'reports_y_axis_field'                 => 'Eixo Y - por favor escolha um dos campos numéricos',
    'reset_password_woops'                 => '<strong> Ooops! </strong> Há problemas com a entrada:',
    'restore'                              => 'Recuperar',
    'sample_answer'                        => 'Respuesta de Ejemplo',
    'sample_category'                      => 'Categoría de ejemplo',
    'sample_question'                      => 'Pregunta de ejemplo',
    'select_all'                           => 'Seleccionar todo',
    'select_crud_placeholder'              => 'Por favor selecione los CRUDs',
    'select_dt_placeholder'                => 'Por favor selecione campos de fecha/hora',
    'select_users_placeholder'             => 'Por favor selecione uno de los usuarios',
    'send'                                 => 'Enviar',
    'serial_number'                        => 'Número de série',
    'simple_user'                          => 'Utilizador Simples',
    'skype'                                => 'Skype',
    'slug'                                 => 'Slug',
    'start_date'                           => 'Data de início',
    'start_time'                           => 'Tempo de início',
    'status'                               => 'Estado',
    'statuses'                             => 'Status',
    'stripe_transactions'                  => 'Classe de transações',
    'subject'                              => 'Assunto',
    'subscription-billing'                 => 'Assinaturas',
    'subscription-payments'                => 'Pagamentos',
    'suffix'                               => 'Sufixo',
    'tag'                                  => 'Tag',
    'tags'                                 => 'Tags',
    'task_management'                      => 'Gestão de tarefas',
    'tasks'                                => 'Tarefas',
    'team-management'                      => 'Equipas',
    'team-management-singular'             => 'Equipa',
    'text'                                 => 'Texto',
    'there_were_problems_with_input'       => 'Há problemas com a entrada',
    'time'                                 => 'Tempo',
    'title'                                => 'Título',
    'transaction_date'                     => 'Data da transação',
    'trash'                                => 'Lixo',
    'update'                               => 'Atualizar',
    'updated_at'                           => 'Atualizado em',
    'upgrade_to_premium'                   => 'Atualizar para Premium',
    'user_actions'                         => 'Ações do utilizador',
    'valid_from'                           => 'Válido de',
    'valid_to'                             => 'Válido até',
    'website'                              => 'Website',
    'when_crud'                            => 'Quando CRUD',
    'whoops'                               => 'Ops!',
    'x_axis_field'                         => 'Campo do eixo X (data/hora)',
    'x_axis_group_by'                      => 'Eixo X agrupar por',
    'y_axis_field'                         => 'Campo do eixo Y',
    'you_have_no_messages'                 => 'Você não possui nenhuma mensagem.',
    'content'                              => 'Conteúdo',
    'no_alerts'                            => 'Nenhum alerta',
    'calendar'                             => 'Calendário',
    'messenger'                            => 'Messenger',
];
