**Instalación de proyecto**

1. Instalar paquete composer con el comando "composer install" dentro del terminal de la carpeta del proyecto.

2. Crear y configurar un nuevo archivo .env a partir de .env.example, insertando las configuraciones de su base de datos.

3. Ejecutamos las migraciones y los seeders, con el comando: php artisan migrate --seed
    (Si ya existiese tablas creadas, se ejecuta "php artisan migrate:fresh --seed" para reiniciar y crear en la BD desde cero)
4. Ejecutamos el comando: "php artisan key:generate"
    (Para generar la APP_KEY en el archivo .env, para iniciar el proyecto)

5. Ejecutamos el comando: "php artisan storage:link"

6. Probamos de manera local con el comando: "php artisan serve" 

<br>
 Admin Login:

Username:	admin@unamba.com <br>
Password:	password 
<br>


