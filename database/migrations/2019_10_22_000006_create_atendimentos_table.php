<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAtendimentosTable extends Migration
{
    public function up()
    {
        Schema::create('atendimentos', function (Blueprint $table) {
            $table->increments('id');

            $table->date('data');

            $table->time('hora');

            $table->float('peso')->nullable();

            $table->bigInteger('talla')->nullable();

            $table->string('presion');
            
            $table->string('fcard');

            $table->string('fresp');

            $table->bigInteger('temperatura');

            $table->longText('observacoes')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
